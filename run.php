<?php

use App\Application;
use App\Entity\BinPacking\Mapper\BinMapper;
use App\Entity\BinPacking\Mapper\ProductMapper;
use App\Service\BinPackingService;
use App\Service\FallbackPackingService;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Psr7\Message;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;

/** @var EntityManager $entityManager */
$entityManager = require __DIR__ . '/src/bootstrap.php';

$request = new Request('POST', new Uri('http://localhost/pack'), ['Content-Type' => 'application/json'], $argv[1]);

// simple DI
$di = [];
$di['binPackingParams'] = [
    'images_background_color' => '255,255,255',
    'images_bin_border_color' => '59,59,59',
    'images_bin_fill_color' => '230,230,230',
    'images_item_border_color' => '22,22,22',
    'images_item_fill_color' => '255,193,6',
    'images_item_back_border_color' => '22,22,22',
    'images_sbs_last_item_fill_color' => '177,14,14',
    'images_sbs_last_item_border_color' => '22,22,22',
    'images_format' => 'svg',
    'images_width' => '50',
    'images_height' => '50',
    'images_source' => 'file',
    'stats' => '0',
    'item_coordinates' => '1',
    'images_complete' => '1',
    'images_sbs' => '1',
    'images_separated' => '0',
    'optimization_mode'=>'bins_number',
    'images_version'=>2,
];
$di[BinMapper::class] = static fn(): BinMapper => new BinMapper;
$di[ProductMapper::class] = static fn(): ProductMapper => new ProductMapper;
$di[BinPackingService::class] = static fn(): BinPackingService  => new BinPackingService(baseUri: 'https://global-api.3dbinpacking.com/', username: 'vladimir.polak@ulekare.cz', apiKey: 'ef909051a739fc1f3ce7faace7d66f54', params: $di['binPackingParams']); // TODO move apiKey + username to .env
$di[FallbackPackingService::class] = static fn(): FallbackPackingService  => new FallbackPackingService;

$application = new Application($entityManager, $di[BinPackingService::class](), $di[FallbackPackingService::class](), $di[BinMapper::class](), $di[ProductMapper::class]());
$response = $application->run($request);

echo "<<< In:\n" . Message::toString($request) . "\n\n";
echo ">>> Out:\n" . Message::toString($response) . "\n\n";
