<?php

namespace App\Entity\BinPacking;
readonly class Bin
{
    public function __construct(
        readonly int   $id,
        readonly float $w,
        readonly float $h,
        readonly float $d,
        readonly float $max_wg,
        readonly float $wg = 0.0,
        readonly bool $vr = true,
        readonly float $cost = 0.0
    ) {

    }
}
