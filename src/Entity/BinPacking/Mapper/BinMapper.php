<?php

namespace App\Entity\BinPacking\Mapper;

use App\Entity\BinPacking\Bin;
use App\Entity\Packaging;

readonly class BinMapper
{
    /**
     * @param Packaging[] $packaging
     * @return Bin[]
     */
    public function mapFromPackaging(array $packaging): array
    {
        return array_map(static fn(Packaging $packaging): Bin => new Bin(
            $packaging->getId(),
            $packaging->getWidth(),
            $packaging->getHeight(),
            $packaging->getLength(),
            $packaging->getMaxWeight()
        ), $packaging);
    }
}
