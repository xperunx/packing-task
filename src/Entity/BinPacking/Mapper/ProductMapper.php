<?php

namespace App\Entity\BinPacking\Mapper;

use App\Entity\BinPacking\Product;
use InvalidArgumentException;
use JsonException;
use Psr\Http\Message\RequestInterface;
use stdClass;

readonly class ProductMapper
{
    public function mapFromRequest(RequestInterface $request): array
    {
        try {
            $contents = json_decode((string)$request->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
            return array_map(static fn(stdClass $item): Product => new Product(
                $item->id,
                $item->width,
                $item->height,
                $item->length,
                $item->weight
            ), $contents->products);
        } catch (JsonException $jsonException) {
            throw new InvalidArgumentException('Invalid request', 0, $jsonException);
        }
    }
}
