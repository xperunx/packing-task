<?php

namespace App\Entity\BinPacking;
readonly class Product
{
    public function __construct(
        readonly int   $id,
        readonly float $w,
        readonly float $h,
        readonly float $d,
        readonly float $wg,
        readonly int $q = 1,
        readonly bool $vr = true,
    ) {

    }
}
