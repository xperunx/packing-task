<?php

namespace App;

use App\Entity\BinPacking\Bin;
use App\Entity\BinPacking\Mapper\BinMapper;
use App\Entity\BinPacking\Mapper\ProductMapper;
use App\Entity\Packaging;
use App\Service\BinPackingService;
use App\Service\FallbackPackingService;
use App\Service\PackingServiceInterface;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Psr7\Response;
use JsonException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use stdClass;

class Application
{

    public function __construct(
        readonly EntityManager     $entityManager,
        readonly PackingServiceInterface $binPackingService,
        readonly PackingServiceInterface $fallbackPackingService,
        readonly BinMapper $binMapper,
        readonly ProductMapper $productMapper
    )
    {

    }

    public function run(RequestInterface $request): ResponseInterface
    {
        try {
            $packaging = $this->entityManager->getRepository(Packaging::class)->findAll();
            $binsPacked = $this->binPackingService->getPackShipment(
                $this->productMapper->mapFromRequest($request),
                $this->binMapper->mapFromPackaging($packaging)
            );

            if (empty($binsPacked) || count($binsPacked) > 1) {
                return new Response(status: 404, body: json_encode(['error' => 'Products can not be packet'], JSON_THROW_ON_ERROR));
            }

            return new Response(status: 200, body: json_encode([
                'width' => $binsPacked[0]->bin_data->w,
                'height' => $binsPacked[0]->bin_data->h,
                'length' => $binsPacked[0]->bin_data->d,
                'weight' => $binsPacked[0]->bin_data->weight,
            ], JSON_THROW_ON_ERROR));
        } catch (JsonException $jsonException) {
            // TODO handle error and log
        }

        // TODO fallback implementation
        return new Response();
    }

}
