<?php

namespace App\Service;

use App\Entity\BinPacking\Bin;
use App\Entity\BinPacking\Product;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Throwable;

readonly class BinPackingService implements PackingServiceInterface
{
    public function __construct(
        private string $baseUri,
        private string $username,
        private string $apiKey,
        private array  $params,
    )
    {

    }

    /**
     * @param Product[] $products
     * @param Bin[] $bins
     * @return array bins packed or empty when cannot be packed, or service is unavailable (todo map result to object)
     */
    public function getPackShipment(array $products, array $bins): array
    {
        try {

        $response = $this->getClient()
            ->send(new Request('POST', '/packer/packIntoMany', [], json_encode([
                'username' => $this->username,
                'api_key' => $this->apiKey,
                'items' => $products,
                'bins' => $bins,
                'params' => $this->params
            ], JSON_THROW_ON_ERROR)));

        $result = json_decode($response->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
        } catch (Throwable $exception) {
            // TODO log error
            return [];
        }

        return $result->response->bins_packed ?? [];
    }

    private function getClient(): Client
    {
        static $client;

        return $client ?? $client = (new Client([
                'base_uri' => $this->baseUri,
                'timeout' => 2.0,
            ]));
    }

}
