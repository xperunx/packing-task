<?php

namespace App\Service;

use App\Entity\BinPacking\Bin;
use App\Entity\BinPacking\Product;

interface PackingServiceInterface
{
    /**
     * @param Product[] $products
     * @param Bin[] $bins
     * @return array bins packed or empty when cannot be packed, or service is unavailable
     */
    public function getPackShipment(array $products, array $bins): array;
}
